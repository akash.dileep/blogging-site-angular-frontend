import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogService } from '../shared/blog.service';

@Component({
  templateUrl: './edit-blog.component.html',
})
export class EditBlogComponent implements OnInit {
  editBlogForm!: FormGroup;
  blog: any;
  blogId: string = '';

  constructor(
    private blogService: BlogService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    //Get blogId
    this.route.params.subscribe(
      (data) => (this.blogId = data.id),
      (err) => console.log(err)
    );

    //Get blog correspond to the blogId
    const data = await this.blogService.getBlogById(this.blogId);
    this.blog = data;
    this.initilizeBlog();
  }

  //Initilize the form to edit the blog
  initilizeBlog() {
    this.editBlogForm = this.formBuilder.group({
      title: [this.blog.title, Validators.required],
      categories: [this.blog.categories, Validators.required],
      content: [this.blog.content, Validators.required],
    });
  }

  //Edit the blog
  async editBlog() {
    let updatedForm = this.editBlogForm.value;

    //Update the form if values changed
    if (
      updatedForm.title != this.blog.title ||
      updatedForm.categories != this.blog.categories ||
      updatedForm.content != this.blog.content
    ) {
      const data = await this.blogService.editBlog(this.blogId, updatedForm);
      this.blog = data;
    }
    // setTimeout(() => {
    //   this.router.navigate(['blog/', this.blogId]);
    // }, 100);
    this.router.navigate(['blog/', this.blogId]);
  }
}
