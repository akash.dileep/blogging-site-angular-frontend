import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BlogModel } from 'src/app/models/blog.model';

@Injectable({
  providedIn: 'root',
})

//Operations on the blog
export class BlogService {
  //Update only for another custom backend
  blogsUrl: string = 'http://localhost:4000/';

  constructor(private http: HttpClient) {}

  //Geting all the blogs
  getBlogs() {
    //Update only for another custom backend
    return this.http.get(this.blogsUrl).toPromise();
  }

  //Get a blog by its id
  getBlogById(id: string) {
    //Update only for another custom backend
    return this.http.get(this.blogsUrl + id).toPromise();
  }

  //Adds a new blog
  addNewBlog(newBlog: BlogModel) {
    //Update only for another custom backend
    return this.http.post(this.blogsUrl + 'new', newBlog).toPromise();
  }

  //Updates the blog with the corresponding id
  editBlog(id: string, editedBlog: BlogModel) {
    //Update only for another custom backend
    return this.http.put(this.blogsUrl + 'edit/' + id, editedBlog).toPromise();
  }

  //Deletes a blog
  deleteBlog(id: string) {
    //Update only for another custom backend
    return this.http.delete(this.blogsUrl + id).toPromise();
  }
}
