import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BlogService } from '../shared/blog.service';

@Component({
  templateUrl: './create-blog.component.html',
})
export class CreateBlogComponent implements OnInit {
  newBlogForm!: FormGroup;
  blog: any;

  constructor(
    private blogService: BlogService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    //Initilize form
    this.newBlogForm = this.formBuilder.group({
      title: ['', Validators.required],
      categories: ['', Validators.required],
      content: ['', Validators.required],
    });
  }

  //Adds a new blog
  async addNewBlog() {
    const data=await this.blogService.addNewBlog(this.newBlogForm.value);
    this.blog = data;
    // setTimeout(() => {
    //   this.router.navigate(['blog/', this.blog._id]);
    // }, 100);
    this.router.navigate(['blog/', this.blog._id]);
  }
}
