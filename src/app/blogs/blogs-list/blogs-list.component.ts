import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BlogService } from '../shared/blog.service';

@Component({
  templateUrl: './blogs-list.component.html',
})

//For displaying list of blogs
export class BlogsListComponent implements OnInit {
  blogs: any;

  constructor(private blogsService: BlogService, private router: Router) {}

  //Get all the blogs
  ngOnInit() {
    setTimeout(async () => {
      const data = await this.blogsService.getBlogs();
      this.blogs = data;
    }, 250);
  }

  //Redirects to create blog form
  newPost() {
    this.router.navigate(['blog/new']);
  }
}
