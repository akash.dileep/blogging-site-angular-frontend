import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogService } from '../shared/blog.service';

@Component({
  templateUrl: './blog-display.component.html',
})

//For displaying a blog
export class BlogDisplayComponent implements OnInit {
  blogId: string = '';
  blog: any;

  constructor(
    private blogService: BlogService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  // Get the blog that has to be displayed
  async ngOnInit(): Promise<void> {
    //Get blogId
    this.route.params.subscribe(
      (data) => (this.blogId = data.id),
      (err) => console.log(err)
    );

    //Get the blog with the corresponding blogId
    const data = await this.blogService.getBlogById(this.blogId);
    this.blog = data;
  }

  //Redirects to edit page
  editBlog() {
    this.router.navigate(['blog/', this.blogId, 'edit']);
  }

  // Deletes a blog
  async deleteBlog() {
    this.router.navigate(['']);
    await this.blogService.deleteBlog(this.blogId);
  }
}
