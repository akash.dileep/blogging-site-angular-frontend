import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogDisplayComponent } from './blogs/blog-display/blog-display.component';
import { BlogsListComponent } from './blogs/blogs-list/blogs-list.component';
import { CreateBlogComponent } from './blogs/create-blog/create-blog.component';
import { EditBlogComponent } from './blogs/edit-blog/edit-blog.component';
import { Error404Component } from './errors/error-404/error-404.component';

const routes: Routes = [
  {
    //For displaying all the blogs (Home Page)
    path: '',
    component: BlogsListComponent,
  },
  {
    //For displaying all the blogs (Home Page)
    path: 'blogs',
    component: BlogsListComponent,
  },
  {
    //For creating a new blog
    path: 'blog/new',
    component: CreateBlogComponent,
  },
  {
    //For displaying a particular blog associated with the id
    path: 'blog/:id',
    component: BlogDisplayComponent,
  },
  {
    //For editing a particular blog associated with the id
    path: 'blog/:id/edit',
    component: EditBlogComponent,
  },
  {
    //For displaying not found error page
    path: 'error/404',
    component: Error404Component,
  },
  {
    //For invalid path redirect to home page
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
