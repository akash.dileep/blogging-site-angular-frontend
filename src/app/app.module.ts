import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogsListComponent } from './blogs/blogs-list/blogs-list.component';
import { BlogService } from './blogs/shared/blog.service';
import { BlogDisplayComponent } from './blogs/blog-display/blog-display.component';
import { Error404Component } from './errors/error-404/error-404.component';
import { CreateBlogComponent } from './blogs/create-blog/create-blog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EditBlogComponent } from './blogs/edit-blog/edit-blog.component';

@NgModule({
  declarations: [
    AppComponent,
    BlogsListComponent,
    BlogDisplayComponent,
    Error404Component,
    CreateBlogComponent,
    EditBlogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [BlogService],
  bootstrap: [AppComponent],
})
export class AppModule {}
