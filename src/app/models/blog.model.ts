export class BlogModel {
  constructor(
    public title: string,
    public categories: string,
    public content: string
  ) {}
}
