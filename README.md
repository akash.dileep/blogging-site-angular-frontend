# Blogging Site - Angular Frontend

It is a blogging site with basic CRUD operations

> **Note:** Its only frontend and **cannot work** without a backend server


## Requirements

-**Node.js:**
You could install the latest version of **Node.js** from [here](https://nodejs.org/en/).

-**Angular CLI:**
You could install the latest version of **Angular CLI**, by typing the following command in the terminal:\
`npm install -g @angular/cli`\
For further information regarding setting up Angular, you can refer to the documentation from [here](https://angular.io/guide/setup-local).

> **Note:** Angular CLI should be installed only after Node.js installation.


## How to Start App

-Clone/Download the files in your local machine.

-After that open the terminal and cd to the **BloogingSite** app folder.

-Now install the required node modules, by typing the following command in the terminal:\
`npm install` or `npm i`

-Now run the dev server by typing the following command in the terminal:\
`ng server` or `npm start`

-The app is running now and to view the app navigate to `http://localhost:4200/` in your browser.


## Setting Backend Server

-You can have a custom backend server particularly made for the following app through this [link](https://gitlab.com/akash.dileep/blogging-site-backend-server).

-For setting the server you should follow the instructions provided in the **README** file of the above backend server.


## Using Your Own Backend Server

-If you are using your own backend server, then update the file **blog.service.ts** present in:\
`src/app/blogs/shared/blog.service.ts`

-Update only:\
`blogsUrl`\
`http.get() parameter`\
`http.post() parameter`\
`http.put() parameter`\
`http.delete() parameter`
